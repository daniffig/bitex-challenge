require 'test_helper'

class UserTest < ActiveSupport::TestCase

  @user_id = 3855
  @issue_id = 6550
  @identification_seed_id = 5794

  def create_user
    Bitex::Api::User.create
  end

  def create_issue_for(user, reason_code = 'new_client')
    issue = Bitex::Api::Issue.new(reason_code: reason_code)
    issue.relationships.user = user
    issue.save

    issue
  end

  def create_natural_docket_seed_for(issue)
    natural_docket_seed = Bitex::Api::NaturalDocketSeed.new(
      first_name: 'Lucio Flavio',
      last_name: 'Di Giacomo Noack',
      nationality: 'AR',
      birth_date: '1988-02-23'
    )
    natural_docket_seed.relationships.issue = issue
    natural_docket_seed.save

    natural_docket_seed
  end

  def create_identification_seed_for(issue)
    identification_seed = Bitex::Api::IdentificationSeed.new(
      identification_kind_code: 'national_id',
      issuer: 'AR',
      number: '33590200'
    )
    identification_seed.relationships.issue = issue
    identification_seed.save

    identification_seed
  end

  def create_domicile_seed_for(issue)
    domicile_seed = Bitex::Api::DomicileSeed.new(
      city: 'La Plata',
      country: 'AR',
      postal_code: '1900',
      street_address: 'Calle 62',
      street_number: '2340',
    )
    domicile_seed.relationships.issue = issue
    domicile_seed.save

    domicile_seed
  end

  def create_attachment_for(seed)
    attachment = Bitex::Api::Attachment.new(
      document: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wlseKgAAAABJRU5ErkJggg==',
      document_file_name: 'pixel.png',
      document_content_type: 'image/png',
      document_size: '68'
    )
    attachment.relationships.attached_to_seed = seed
    attachment.save

    attachment
  end

  test "should create a user with all its data" do
    user = create_user
    assert_not_nil user.id, 'User could not be created'

    issue = create_issue_for user
    assert_not_nil issue.id, 'Issue could not be created'

    natural_docket_seed = create_natural_docket_seed_for issue
    assert_not_nil natural_docket_seed.id, 'Natural docket seed could not be created'

    identification_seed = create_identification_seed_for issue
    assert_not_nil identification_seed.id, 'Identification seed could not be created'

    domicile_seed = create_domicile_seed_for issue
    assert_not_nil domicile_seed.id, 'Domicile seed could not be created'

    identification_attachment = create_attachment_for identification_seed
    assert_not_nil identification_attachment.id, 'Identification attachment seed could not be created'

    domicile_attachment = create_attachment_for domicile_seed
    assert_not_nil domicile_attachment.id, 'Domicile attachment seed could not be created'
  end

  # test "should create an user" do
  #   user = Bitex::Api::User.create

  #   assert user.id
  # end

  # test "should create an issue for an user" do
  #   user = Bitex::Api::User.one(@user_id)

  #   issue = Bitex::Api::Issue.new(reason_code: 'new_client')
  #   issue.relationships.user = user
  #   issue.save

  #   assert issue.id
  # end

  # test "should create a natural docket seed for an issue" do
  #   issue = Bitex::Api::Issue.one(@issue_id)

  #   natural_docket_seed = Bitex::Api::NaturalDocketSeed.new(
  #     first_name: 'Lucio Flavio',
  #     last_name: 'Di Giacomo Noack',
  #     nationality: 'AR',
  #     birth_date: '1988-02-23'
  #   )
  #   natural_docket_seed.relationships.issue = issue
  #   natural_docket_seed.save

  #   assert natural_docket_seed.id
  # end

  # test "should create an identification seed for an issue" do
  #   issue = Bitex::Api::Issue.one(@issue_id)

  #   identification_seed = Bitex::Api::IdentificationSeed.new(
  #     identification_kind_code: 'national_id',
  #     issuer: 'AR',
  #     number: '33590200'
  #   )
  #   identification_seed.relationships.issue = issue
  #   identification_seed.save

  #   assert identification_seed.id
  # end

  # test "should create an domicile seed for an issue" do
  #   issue = Bitex::Api::Issue.one(@issue_id)

  #   domicile_seed = Bitex::Api::DomicileSeed.new(
  #     city: 'La Plata',
  #     country: 'AR',
  #     postal_code: '1900',
  #     street_address: 'Calle 62',
  #     street_number: '2340'
  #   )
  #   domicile_seed.relationships.issue = issue
  #   domicile_seed.save

  #   assert domicile_seed.id
  # end
end
