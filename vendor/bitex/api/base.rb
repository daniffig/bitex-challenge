module Bitex::Api
  class Base < JsonApiClient::Resource
    # set the api base url in an abstract base class
    self.site = "https://sandbox.bitex.la/api"
    self.connection_class = Bitex::Api::Connection

    def self.one(pk)
      self.find(pk).first
    end
  end
end