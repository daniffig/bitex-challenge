module Bitex::Api
  class Connection < JsonApiClient::Connection   
    
    # def initialize(options = {})
    #   super(options)
      
    #   self.resource_class = options.fetch(:resource_class)
    #   self.authentication_path = options.fetch(:authentication_path)
    #   self.authentication_body = options.fetch(:authentication_body)
      
    #   self.authorization_bearer = self.resource_class.authorization_bearer

    #   self.faraday.ssl[:verify] = false

    #   self
    # end

    # TODO Upgrade this in the future, current version is 1.5.3
    def run(request_method, path, params = nil) 
      params[:headers]['Authorization'] = ENV['BITEX_API_TOKEN']
      params[:headers]['Version'] = '2.1'

      super(request_method, path, params)
    end
  end
end  
