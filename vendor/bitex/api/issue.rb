module Bitex::Api
  class Issue < Base
    belongs_to :user, shallow_path: true

    has_many :natural_docket_seeds

    property :reason_code, type: :string, default: 'new_client'

    def self.create_for(user, reason_code: 'new_client')
      issue = Bitex::Api::Issue.new(reason_code: reason_code)
      issue.relationships.user = user
      issue.save

      issue
    end
  end
end