module Bitex::Api
  class DomicileSeed < Base
    belongs_to :issue, shallow_path: true
    
    has_many :attachments

    property :city, type: :string
    property :country, type: :string
    property :floor, type: :string, default: '0'
    property :postal_code, type: :string
    property :street_address, type: :string
    property :street_number, type: :string

    def self.create_for(issue, user)
      domicile_seed = Bitex::Api::DomicileSeed.new(
        city: user.domicile_city,
        country: user.domicile_country,
        postal_code: user.domicile_postal_code,
        street_address: user.domicile_street_address,
        street_number: user.domicile_street_number,
        floor: user.domicile_floor
      )
      domicile_seed.relationships.issue = issue
      domicile_seed.save
  
      domicile_seed
    end
  end
end