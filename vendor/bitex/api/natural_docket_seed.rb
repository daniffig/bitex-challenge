module Bitex::Api
  class NaturalDocketSeed < Base
    belongs_to :issue, shallow_path: true

    property :first_name, type: :string
    property :last_name, type: :string
    property :nationality, type: :string
    property :gender_code, type: :string
    property :marital_status_code, type: :string
    property :politically_exposed, type: :boolean

    # TODO
    # We could use a Date parse with this property.
    property :birth_date, type: :string

    def self.create_for(issue, user)
      natural_docket_seed = Bitex::Api::NaturalDocketSeed.new(
        first_name: user.first_name,
        last_name: user.last_name,
        nationality: user.nationality,
        birth_date: user.birth_date
      )
      natural_docket_seed.relationships.issue = issue
      natural_docket_seed.save
  
      natural_docket_seed
    end
  end
end