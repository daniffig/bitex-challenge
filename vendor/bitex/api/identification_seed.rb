module Bitex::Api
  class IdentificationSeed < Base
    belongs_to :issue, shallow_path: true

    has_many :attachments

    property :identification_kind_code, type: :string
    property :issuer, type: :string
    property :number, type: :string

    def self.create_for(issue, user)
      identification_seed = Bitex::Api::IdentificationSeed.new(
        identification_kind_code: 'national_id',
        issuer: 'AR',
        number: user.id_number
      )
      identification_seed.relationships.issue = issue
      identification_seed.save
  
      identification_seed
    end
  end
end