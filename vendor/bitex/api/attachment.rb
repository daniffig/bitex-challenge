module Bitex::Api
  class Attachment < Base
    belongs_to :attached_to_seed, shallow_path: true

    property :document, type: :string
    property :document_file_name, type: :string
    property :document_content_type, type: :string
    property :document_size, type: :string

    def self.create_for(seed, user, field)
      attachment = Bitex::Api::Attachment.new(
        # document: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wlseKgAAAABJRU5ErkJggg==',
        # document_file_name: 'pixel.png',
        # document_content_type: 'image/png',
        # document_size: '68'
        document: user.try("#{field}_content".to_sym),
        document_file_name: user.try("#{field}_name".to_sym),
        document_content_type: user.try("#{field}_type".to_sym),
        document_size: user.try("#{field}_type".to_sym).to_s
      )
      attachment.relationships.attached_to_seed = seed
      attachment.save
  
      attachment
    end
  end
end
