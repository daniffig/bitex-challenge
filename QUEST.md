Mario Dal Lago <mdallago@bitex.la>
	
mar, 30 mar. 16:16 (hace 7 días)
	
para mí, Guadalupe, Tomas
Hola Lucio,

Te comparto el ejercicio que te comentamos. Cualquier duda que tengas avisanos.

Saludos
---------

Una parte importante de las integraciones de Bitex es el cumplimiento de las leyes de prevención de lavado de dinero, que requieren que la empresa lleve adelante una política de "Conoce a tu cliente".

Para hacerlo, Bitex requiere datos personales de todas las personas físicas y jurídicas a quienes cobra y paga.

Bitex tiene una API donde nuestros integradores pueden crear nuevos Usuarios y dar de alta sus datos personales. La documentación está acá: https://api.bitex.la/docs/users/welcome/

El ejercicio que proponemos es que hagas una interfaz web donde un usuario pueda enviar a Bitex los siguientes datos personales:
- Nombre y apellido
- Fecha de nacimiento.
- Nacionalidad
- Documento nacional de identidad.
- Una imágen de su documento nacional de identidad.
- Domicilio completo.
- Una imágen de una prueba de domicilio.

Tenés libertad de elegir todas las tecnologías y arquitectura que prefieras, así como el estilo visual.
Ante cualquier duda sobre la lógica de negocios o el scope del ejercicio, tenés libertad de adivinar.
No hay límite de tiempo, decinos vos cuanto crees que te tomaría, o que tiempo tenés para dedicarle.
El objetivo es que podamos aprender sobre como trabajás y charlar sobre la experiencia de hacer el ejercicio. No es excluyente que quede funcionando perfecto, y nos gustaría también saber sobre cosas que hubieras preferido implementar y no hayas podido por falta de tiempo.

Te creamos una cuenta en sandbox.bitex.la y le dimos los permisos de 'Master User' para que puedas hacer este ejercicio.

El API key es:
3c7e8f1d45bc7bce3753296294eca705961c2f21b8c3664c38eb04397bebb8394f494181d9fc31f9
