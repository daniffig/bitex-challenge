# Changelog

## [0.1.0] - 2021-04-09
### Added
- Added Bitex API model.
- Added User model.
- Added Country model.
- Added User validations.
- Added Bitex User tests.

### Changed

### Fixed