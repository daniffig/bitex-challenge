require_relative "boot"

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
# require "active_record/railtie"
# require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
# require "action_mailbox/engine"
# require "action_text/engine"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module App
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # config.eager_load_paths += Dir["#{config.root}/app/bitex/**/"]
    # config.eager_load_paths << Rails.root.join('app', 'bitex', 'api', '**')
    config.eager_load_paths << Rails.root.join("vendor")

    config.time_zone = 'America/Argentina/Buenos_Aires'

    config.i18n.default_locale = :'es-AR'
    config.i18n.available_locales = :'es-AR', :en

    config.x.application_name = 'Bitex Challenge'
    config.x.version = '0.1.0'
  end
end
