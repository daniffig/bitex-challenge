<!-- https://github.com/todomd/todo.md -->

# Bitex Challenge

### Todo
- [ ] Create a Changelog.
- [ ] Format frontend birthDate.
- [ ] Model Domicile as class related to User.
- [ ] Should we ask for a door number?
- [ ] Validate attachments file size.
- [ ] Filter attachments content from logs.
- [ ] Improve model tests.
- [ ] Add User tests.
- [ ] Add numericality validations.
- [ ] Refactor frontend components.
- [ ] Fix frontend birthDate validation bug.
- [ ] Show success/error better.

### In Progress

### Done
- [x] Improve routes.