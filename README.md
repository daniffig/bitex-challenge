# Bitex Challenge (ldgnoack)

## What does this app do?
It provides a web form developed with Vue.js & Vuetify to complete a user's profile according to Bitex KYC policies. The user input is validated locally then sent to the app server through a JSONAPI endpoint. After server-side validation it is delivered to Bitex Sandbox API server using several calls. If everything is OK it adds a fake id to the User instance so to make it believe the data was persisted.

## Requirements
* ruby, -> '2.6.6'
* docker & docker-compose (if you want to test it easily)

## Installation
### Standalone
```
bundle install && yarn install && rails s
```

If you run the application this way you will need a master.key file to decode credentials.yml.enc. You can copy it from the environment key in the docker-compose.yml file, or if you are lazy you can just run this.

```
echo 55104b2c20be7e20f0894f5972e04316 > config/master.key
```

### Docker-compose
This will be bound to ports 3000 & 3035 so try to free them before running the next command.
```
docker-compose up
```

Your instance is not available at http://localhost:3000

## Online demo
You can temporally try this app at https://bitex-challenge.noack.net.ar


## More docs
* Bitex Api Docs - https://developers.bitex.la/?version=latest