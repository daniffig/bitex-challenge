import Vue from 'vue'

Vue.prototype.$setFileData = function (file, object, attribute) {
  if (file) {
    const reader = new FileReader()

    reader.onloadend = function (event) {
      object[attribute + 'Content'] = event.target.result
      object[attribute + 'Name'] = file.name
      object[attribute + 'Size'] = file.size
      object[attribute + 'Type'] = file.type
    }

    reader.readAsDataURL(file)
  }
}