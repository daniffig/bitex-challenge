import Vue from "vue";

require("moment/locale/es");

const moment = require("moment");

Vue.use(require("vue-moment"), {
  moment,
});

Vue.prototype.$formatDate = function (date) {
  if (!date) return null
  
  return this.$moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY')
}
