import New from "@/pages/users/New";

const baseRoutes = [
  {
    path: "/",
    name: "root",
    component: New,
    meta: { resource: "User", action: "create" },
  },
];

export default baseRoutes;
