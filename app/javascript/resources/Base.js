import APILibrary from "@/resources/APILibrary";

class Base extends APILibrary.Base {
  static toUnderscore(text) {
    return text
      .split(/(?=[A-Z])/)
      .join("_")
      .toLowerCase();
  }

  getErrors() {
    const output = {};
    const backendErrors = this.__errors.__errors;
    const toUnderscore = this.klass().toUnderscore;

    Object.keys(backendErrors).forEach((key) => {
      const errors = [];
      backendErrors[key].forEach((value) => {
        errors.push(
          value.message.replace(`${toUnderscore(value.field)} - `, "")
        );
      });

      output[key] = errors.toString();

      // TODO Find a way to handle erros when have relationships
      output[`${key}Id`] = errors.toString();
    });

    return output;
  }

  getErrorsAsString() {
    const output = {};
    const backendErrors = this.__errors.__errors;
    const toUnderscore = this.klass().toUnderscore;

    Object.keys(backendErrors).forEach((key) => {
      const errors = [];
      backendErrors[key].forEach((value) => {
        errors.push(
          value.message.replace(`${toUnderscore(value.field)} - `, "")
        );
      });
      output[key] = errors.toString();
    });

    return Object.values(output).join(", ");
  }
}

export default Base;
