import APILibrary from "@/resources/APILibrary";
import Base from "@/resources/Base";

class User extends Base {
  // required by active-resource
  // https://github.com/nicklandgrebe/active-resource.js/issues/47
  static className = "User";
  static queryName = "users";

  // required by @casl/ability
  // https://stackoverflow.com/questions/57677510/casl-is-not-working-properly-in-vue-production-mode
  static modelName = "User";

  static define() {
    this.attributes(
      "firstName",
      "lastName",
      "birthDate",
      "nationality",
      "idNumber",
      "idAttachmentFrontFileContent",
      "idAttachmentFrontFileName",
      "idAttachmentFrontFileSize",
      "idAttachmentFrontFileType",
      "idAttachmentBackFileContent",
      "idAttachmentBackFileName",
      "idAttachmentBackFileSize",
      "idAttachmentBackFileType",
      "domicileCity",
      "domicileCountry",
      "domicileFloor",
      "domicilePostalCode",
      "domicileStreetAddress",
      "domicileStreetNumber",
      "domicileAttachmentFileContent",
      "domicileAttachmentFileName",
      "domicileAttachmentFileSize",
      "domicileAttachmentFileType",
    );
  }
}

export default APILibrary.createResource(User);
