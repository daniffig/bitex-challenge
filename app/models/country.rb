class Country
  include ActiveModel::Model

  attr_accessor :name_en, :name_es, :dial_code, :code

  COUNTRIES_FILE = Rails.root.join('app', 'javascript', 'data', 'countries.json').freeze

  def self.all
    @countries ||= load_countries
  end

  def self.codes
    all.collect(&:code)
  end

  private

  def self.load_countries
    JSON.parse(File.read(COUNTRIES_FILE)).collect { |country| Country.new(country) }
  end
end