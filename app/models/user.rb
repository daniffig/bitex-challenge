class User
  include ActiveModel::Model

  attr_accessor :id,
    :first_name,
    :last_name,
    :birth_date,
    :nationality,
    :id_number,
    :id_attachment_front_file_content,
    :id_attachment_front_file_name,
    :id_attachment_front_file_size,
    :id_attachment_front_file_type,
    :id_attachment_back_file_content,
    :id_attachment_back_file_name,
    :id_attachment_back_file_size,
    :id_attachment_back_file_type,
    :domicile_street_address,
    :domicile_street_number,
    :domicile_floor,
    :domicile_city,
    :domicile_postal_code,
    :domicile_country,
    :domicile_attachment_file_content,
    :domicile_attachment_file_name,
    :domicile_attachment_file_size,
    :domicile_attachment_file_type

  validates_presence_of :first_name,
    :last_name,
    :birth_date,
    :nationality,
    :id_number,
    :id_attachment_front_file_content,
    :id_attachment_front_file_name,
    :id_attachment_front_file_size,
    :id_attachment_front_file_type,
    :id_attachment_back_file_content,
    :id_attachment_back_file_name,
    :id_attachment_back_file_size,
    :id_attachment_back_file_type,
    :domicile_street_address,
    :domicile_street_number,
    :domicile_city,
    :domicile_postal_code,
    :domicile_country,
    :domicile_attachment_file_content,
    :domicile_attachment_file_name,
    :domicile_attachment_file_size,
    :domicile_attachment_file_type

  validates_date :birth_date, before: lambda { 18.years.ago }

  validates_inclusion_of :nationality, in: Country.codes
  validates_inclusion_of :domicile_country, in: Country.codes

  def initialize(attributes = {})
    attributes.each do |attribute, value|
      self.send("#{attribute.to_sym}=", value)
    end

    @bitex_user = nil
    @bitex_issues = []
    @bitex_seeds = []
    @bitex_attachments = []
  end

  def save
    return false unless self.valid?

    create_user
    create_issue
    create_seeds
    create_attachments

    self
  end

  private

  def create_user
    @bitex_user = Bitex::Api::User.create
  end

  def create_issue
    @bitex_issues << Bitex::Api::Issue.create_for(@bitex_user, reason_code: 'new_client')
  end

  def create_seeds
    @bitex_seeds << Bitex::Api::NaturalDocketSeed.create_for(@bitex_issues.first, self)
    @bitex_seeds << Bitex::Api::IdentificationSeed.create_for(@bitex_issues.first, self)
    @bitex_seeds << Bitex::Api::DomicileSeed.create_for(@bitex_issues.first, self)
  end

  def create_attachments
    @bitex_attachments << Bitex::Api::Attachment.create_for(@bitex_seeds.second, self, 'id_front_attachment_file')
    @bitex_attachments << Bitex::Api::Attachment.create_for(@bitex_seeds.second, self, 'id_back_attachment_file')
    @bitex_attachments << Bitex::Api::Attachment.create_for(@bitex_seeds.third, self, 'domicile_attachment_file')
  end
end