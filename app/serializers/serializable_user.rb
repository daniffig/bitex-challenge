class SerializableUser < JSONAPI::Serializable::Resource
  type 'user'
  
  attributes :first_name, :last_name
end