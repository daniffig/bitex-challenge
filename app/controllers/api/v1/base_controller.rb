class Api::V1::BaseController < ApiController
  include ActionController::RequestForgeryProtection

  protect_from_forgery with: :exception
end
