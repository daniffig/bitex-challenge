class Api::V1::UsersController < Api::V1::BaseController
  def create
    user = User.new(user_params)

    if user.save
      render jsonapi: user
    else
      render jsonapi_errors: user.errors, status: :unprocessable_entity
    end
  end

  def jsonapi_pointers
    {
      first_name: '/data/attributes/firstName',
      last_name: '/data/attributes/lastName',
      birth_date: '/data/attributes/birthDate',
      nationality: '/data/attributes/nationality',
      id_number: '/data/attributes/idNumber',
      id_attachment_front_file_content: '/data/attributes/idAttachmentFrontFileContent',
      id_attachment_front_file_name: '/data/attributes/idAttachmentFrontFileName',
      id_attachment_front_file_size: '/data/attributes/idAttachmentFrontFileSize',
      id_attachment_front_file_type: '/data/attributes/idAttachmentFrontFileType',
      id_attachment_back_file_content: '/data/attributes/idAttachmentFrontFileContent',
      id_attachment_back_file_name: '/data/attributes/idAttachmentFrontFileName',
      id_attachment_back_file_size: '/data/attributes/idAttachmentFrontFileSize',
      id_attachment_back_file_type: '/data/attributes/idAttachmentFrontFileType',
      domicile_street_address: '/data/attributes/domicileStreetAddress',
      domicile_street_number: '/data/attributes/domicileStreetNumber',
      domicile_floor: '/data/attributes/domicileFloor',
      domicile_city: '/data/attributes/domicileCity',
      domicile_postal_code: '/data/attributes/domicilePostalCode',
      domicile_country: '/data/attributes/domicileCountry',
      domicile_attachment_file_content: '/data/attributes/domicileAttachmentFileContent',
      domicile_attachment_file_name: '/data/attributes/domicileAttachmentFileName',
      domicile_attachment_file_size: '/data/attributes/domicileAttachmentFileSize',
      domicile_attachment_file_type: '/data/attributes/domicileAttachmentFileType'
    }
  end

  private

  def user_params
    params.require(:data).require(:attributes).permit(
      :first_name,
      :last_name,
      :birth_date,
      :nationality,
      :id_number,
      :id_attachment_front_file_content,
      :id_attachment_front_file_name,
      :id_attachment_front_file_size,
      :id_attachment_front_file_type,
      :id_attachment_back_file_content,
      :id_attachment_back_file_name,
      :id_attachment_back_file_size,
      :id_attachment_back_file_type,
      :domicile_street_address,
      :domicile_street_number,
      :domicile_floor,
      :domicile_city,
      :domicile_postal_code,
      :domicile_country,
      :domicile_attachment_file_content,
      :domicile_attachment_file_name,
      :domicile_attachment_file_size,
      :domicile_attachment_file_type
    )
  end
end
